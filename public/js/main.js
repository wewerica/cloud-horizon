jQuery(function(){
    var enableDays = '';
    var doctor_id = '';
    
    $('#doctor_id').on('change', function(){
        doctor_id = $(this).val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type:'POST',
            url:'/fetchEnabledDates',
            data:{ doctor_id:doctor_id },
            success:function(data){
                enableDays = data;
                $("#datepicker").val('');
            }
        });

    });
    
    function enableAllTheseDays(date) {
        var sdate = $.datepicker.formatDate( 'yy-mm-dd', date)
        
        if($.inArray(sdate, enableDays) != -1) {
            return [true];
        }
        return [false];
    }
    
    $('#datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        beforeShowDay: enableAllTheseDays
    });
    
})