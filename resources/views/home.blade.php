@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('fetchAppointmentsByDoctorId') }}">
                        @csrf
                        <select name="doctor_id" id="doctor_id" class="form-control">
                            <option value="">Select doctor</option>
                            @foreach($doctors as $doctor)
                                <option value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                            @endforeach
                        </select>
                        
                        <input name="start_date" id="datepicker" class="form-control"/>

                        <button type="submit" class="btn btn-success">Filter</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                {{ old('doctor_id') }}
                @if(isset($appointments))
                    <div class="card-body">
                        <table id="appointments" class="table table-striped">
                            <tr>
                                <th>Patient name</th>
                                <th>Start date & time</th>
                                <th>Specialty</th>
                                <th>Doctor</th>
                            </tr>

                            @foreach($appointments as $appointment)
                                <tr>
                                    <td>{{ $appointment->patient->name }} ( Age: {{ \Carbon\Carbon::parse($appointment->patient->date_of_birth)->age }} )</td>
                                    <!-- <td>{{ $appointment->start_date }} @ {{ $appointment->start_time }}</td> -->
                                    <td>{{ \Carbon\Carbon::parse($appointment['start_date'])->format('m/d/Y') }} @ {{ \Carbon\Carbon::parse($appointment['start_time'])->format('h:m:s a') }}</td>
                                    <td>{{ $appointment->specialty->name }}</td>
                                    <td>{{ $appointment->doctor->name }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection