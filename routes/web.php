<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/fetchAppointments', 'AppointmentController@fetchAppointments')->name('fetchAppointments');


Route::post('/fetchEnabledDates', 'AppointmentController@fetchEnabledDates');
Route::post('/fetchAppointmentsByDoctorId', 'AppointmentController@fetchAppointmentsByDoctorId')->name('fetchAppointmentsByDoctorId');
