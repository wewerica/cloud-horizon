<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'patient_id', 'doctor_id', 'clinic_id', 'specialty_id', 'start_date', 'start_time', 'booked_at'
    ];

    /**
     * Save a new model and return the instance.
     *
     * @param  array  $attributes
     * @return static
     */
    public static function create(array $attributes = array()) {
        if(Appointment::where('id', $attributes['id'])->first() == null){
            $appointment = new Appointment($attributes);

            $appointment->patient_id = $attributes['patient']['id'];
            $appointment->doctor_id = $attributes['doctor']['id'];
            $appointment->clinic_id = $attributes['clinic']['id'];
            $appointment->specialty_id = $attributes['specialty']['id'];

            $appointment->start_date = \Carbon\Carbon::parse($appointment['start_date'])->format('Y-m-d');
            $appointment->start_time = \Carbon\Carbon::parse($appointment['start_time'])->format('H:i:s');

            $appointment->save();

            return $appointment;
        }
    }

    public function patient()
    {
        return $this->belongsTo('App\Patient');
    }

    public function doctor()
    {
        return $this->belongsTo('App\Doctor');
    }

    public function clinic()
    {
        return $this->belongsTo('App\Clinic');
    }

    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }

}
