<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use GuzzleHttp\Client;

use Mtownsend\XmlToArray\XmlToArray;

use \App\Appointment;
use \App\Patient;
use \App\Doctor;
use \App\Clinic;
use \App\Specialty;

class FetchAppointments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:appointments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily fetch new appointments from API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $period = CarbonPeriod::create(Carbon::now(), '2 days', Carbon::now()->add('1 month'));
        
        $dates = $period->toArray();

        foreach($dates as $date){
            /**
             * Fetch data from API
             * username and password are stored in .env file
             */
            $client = new Client();
            $res = $client->request('GET', 'http://ch-api-test.herokuapp.com//xml?from='.$date->format('Y-m-d'), [
                'auth' => [
                    env('API_USER'), 
                    env('API_PASS'), 
                ]
            ]);
            
            /**
             * If get status 200(OK)
             *  parse XML to array and store to database
             */
            if($res->getStatusCode() === 200){
                $data = $res->getBody();
                $appointments = XmlToArray::convert($data);

                foreach($appointments['appointment'] as $appointment){
                    /**
                     * don't import canceled appointments
                     */
                    if($appointment['cancelled'] != 1){

                        $patient = Patient::firstOrCreate(
                            ['id' => $appointment['patient']['id']],
                            [
                                'name' => $appointment['patient']['name'],
                                'date_of_birth' => Carbon::createFromFormat('d/m/Y', $appointment['patient']['date_of_birth']),
                                'sex' => $appointment['patient']['sex']
                            ]
                        );

                        $doctor = Doctor::firstOrCreate(
                            ['id' => $appointment['doctor']['id']],
                            [
                                'name' => $appointment['doctor']['name']
                            ]
                        );

                        $clinic = Clinic::firstOrCreate(
                            ['id' => $appointment['clinic']['id']],
                            [
                                'name' => $appointment['clinic']['name']
                            ]
                        );

                        $specialty = Specialty::firstOrCreate(
                            ['id' => $appointment['specialty']['id']],
                            [
                                'name' => $appointment['specialty']['name']
                            ]
                        );

                        $appointment = Appointment::create($appointment);
                    }
                }
            }
        }

        $this->info('All appointments have been importend!');
    }
}
