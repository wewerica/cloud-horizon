<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Doctor;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $doctors = Doctor::all();
        return view('home', compact('doctors'));
    }
}
