<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use GuzzleHttp\Client;

use Mtownsend\XmlToArray\XmlToArray;

use \App\Appointment;
use \App\Patient;
use \App\Doctor;
use \App\Clinic;
use \App\Specialty;



class AppointmentController extends Controller
{
    
    public function index(){

    }

    public function fetchAppointments(){

        $period = CarbonPeriod::create(Carbon::now(), '2 days', Carbon::now()->add('1 month'));
        
        $dates = $period->toArray();

        foreach($dates as $date){
            /**
             * Fetch data from API
             * username and password are stored in .env file
             */
            $client = new Client();
            $res = $client->request('GET', 'http://ch-api-test.herokuapp.com//xml?from='.$date->format('Y-m-d'), [
                'auth' => [
                    env('API_USER'), 
                    env('API_PASS'), 
                ]
            ]);
            
            /**
             * If get status 200(OK)
             *  parse XML to array and store to database
             */
            if($res->getStatusCode() === 200){
                $data = $res->getBody();
                $appointments = XmlToArray::convert($data);

                foreach($appointments['appointment'] as $appointment){
                    /**
                     * don't import canceled appointments
                     */
                    if($appointment['cancelled'] != 1){

                        $patient = Patient::firstOrCreate(
                            ['id' => $appointment['patient']['id']],
                            [
                                'name' => $appointment['patient']['name'],
                                'date_of_birth' => Carbon::createFromFormat('d/m/Y', $appointment['patient']['date_of_birth']),
                                'sex' => $appointment['patient']['sex']
                            ]
                        );

                        $doctor = Doctor::firstOrCreate(
                            ['id' => $appointment['doctor']['id']],
                            [
                                'name' => $appointment['doctor']['name']
                            ]
                        );

                        $clinic = Clinic::firstOrCreate(
                            ['id' => $appointment['clinic']['id']],
                            [
                                'name' => $appointment['clinic']['name']
                            ]
                        );

                        $specialty = Specialty::firstOrCreate(
                            ['id' => $appointment['specialty']['id']],
                            [
                                'name' => $appointment['specialty']['name']
                            ]
                        );

                        $appointment = Appointment::create($appointment);
                    }
                }
            }
        }
    }

    public function fetchEnabledDates(Request $request){
        $dates = Appointment::where('doctor_id', $request->doctor_id)->distinct()->get(['start_date']);
        
        foreach($dates as $date){
            $enabled_dates[] = $date->start_date;
        }
        
        return $enabled_dates;

    }
    public function fetchAppointmentsByDoctorId(Request $request){
        $doctors = Doctor::all();

        $appointments = Appointment::where('doctor_id', $request->doctor_id)->where('start_date', $request->start_date)->get();

        if(\Auth::guest()){
            foreach($appointments as $k=>$appointment){
                if(\Carbon\Carbon::parse($appointment->patient->date_of_birth)->age < 18){
                    unset($appointments[$k]);
                }
            }
        }
        
        return view('home', compact('doctors', 'appointments'));
    }
}
